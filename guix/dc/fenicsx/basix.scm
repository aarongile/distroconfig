;; -*- mode: scheme; fill-column: 80; comment-column: 50; -*-
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Copyright © 2025 Aron Gile <aronggile@gmail.com>

(define-module (dc fenicsx basix)
  #:use-module (dc fenicsx ufl)
  #:use-module (dc python nanobind)
  #:use-module (gnu packages)
  #:use-module (gnu packages check)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1))

;;; febics-basix C++
(define-public fenicsx-basix
  (package
   (name "fenicsx-basix")
   (version "0.10.0.0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/FEniCS/basix.git")
           (commit  "370d3fdc7e5e3b0a223bb45c6bbf598a183bce1a")))
     (file-name (git-file-name name version))
     (sha256 (base32 "17na5igwdwl923zkci0id5q6cnbyiq38ny239v3660fd809b5xaz"))))
   (native-inputs (list catch-framework pkg-config))
   (inputs   (list openblas))
   (build-system cmake-build-system)
   (arguments
    (list
     #:configure-flags
     #~`("-DCMAKE_BUILD_TYPE=Release")
     #:phases
     #~(modify-phases
        %standard-phases

        ;; change-directory
        (add-before
         'configure 'change-directory
         (lambda _
           (display "changing directory to: cpp")
           (chdir "cpp")))

        ;; don't run tests
        (delete 'check))))
   (license license:expat)
   (home-page "https://fenicsproject.org")

   (synopsis  "Basix is a finite element definition
and tabulation runtime library. It is part of FEniCSx project. ")

   (description "Basix is a finite element definition and
 tabulation runtime library.Basix allows users to:
@itemize
@item evaluate finite element basis functions and their derivatives at a set of points;
@item access geometric and topological information about reference cells;
@item apply push forward and pull back operations to map data between a reference cell
and a physical cell;
@item permute and transform DOFs to allow higher-order elements
to be use on arbitrary meshes; and interpolate into and between
finite element spaces.")))

;;; fenicsx-basix python
(define-public python-fenicsx-basix
  (package/inherit
   fenicsx-basix
   (name "python-fenicsx-basix")
   (build-system pyproject-build-system)
   (native-inputs
    (modify-inputs
     (package-native-inputs fenicsx-basix)
     (prepend
      cmake
      python
      python-scikit-build-core
      python-scikit-build
      python-pip
      ;;testing
      python-mypy
      python-sympy
      python-scipy
      python-matplotlib
      fenicsx-basix)))
   (propagated-inputs
    (list fenicsx-basix
          python-numpy
          python-fenicsx-ufl
          python-nanobind-v2.5))
   (inputs
    (modify-inputs
     (package-inputs fenicsx-basix)
     (prepend fenicsx-basix)))
   (arguments
    (list
     #:phases
     #~(modify-phases
        %standard-phases
        (add-before
         'build 'change-directory
         (lambda _
           (display "changing directory to: python")
           (chdir "python")))
        (replace
         'check (lambda _
                  (display "changing directory to: python")
                  (chdir "../")
                  (invoke "python3" "-m" "pytest" "test/"))))))))
