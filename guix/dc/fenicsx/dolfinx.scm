;; -*- mode: scheme; fill-column: 80; comment-column: 50; -*-
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Copyright © 2025 Aron Gile <aronggile@gmail.com>

(define-module (dc fenicsx dolfinx)
  #:use-module (dc fenicsx basix)
  #:use-module (dc fenicsx ffcx)
  #:use-module (dc fenicsx ufl)
  #:use-module (dc python nanobind)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages check)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages logging)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-science)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1))

(define-public metis32
  (package
   (inherit metis-5.2)
   (name "metis32")
   (arguments
    (list
     #:phases
     #~(modify-phases
        %standard-phases
        (delete    'prepare-cmake)
        (add-after 'unpack 'patch-source
                   (lambda _
                     ;;patch: include/metis.h
                     ;;//#define IDXTYPEWIDTH 32
                     (substitute*
                      "include/metis.h"
                      (("^//#define[ \t]+IDXTYPEWIDTH[ \t]+32")  "#define IDXTYPEWIDTH 32" )
                      (("^//#define[ \t]+REALTYPEWIDTH[ \t]+32") "#define REALTYPEWIDTH 32" ))
                     ;;patch: CMAKELists.txt
                     (substitute* "CMakeLists.txt" (("build/xinclude") "include"))
                     ;;delete the freaking insufferable make file
                     (delete-file "Makefile")))
        (replace   'configure
                   (lambda* (#:key inputs outputs #:allow-other-keys)
                     (let ((source-dir    (getcwd))
                           (build-dir      "build-dir")
                           (install-prefix (assoc-ref outputs "out"))
                           (gklib-path     (assoc-ref inputs  "gklib")))
                       (mkdir-p build-dir)
                       (chdir   build-dir)
                       (invoke "cmake"
                               source-dir
                               "-DCMAKE_VERBOSE_MAKEFILE=1"
                               (string-append "-DCMAKE_INSTALL_PREFIX=" install-prefix)
                               (string-append "-DGKLIB_PATH=" gklib-path)))))
        (delete    'check))))))

(define-public metis64
  (package
   (inherit metis-5.2)
   (name "metis64")
   (arguments
    (list
     #:phases
     #~(modify-phases
        %standard-phases
        (delete    'prepare-cmake)
        (add-after 'unpack 'patch-source
                   (lambda _
                     ;;patch: include/metis.h
                     ;;//#define IDXTYPEWIDTH 32
                     (substitute*
                      "include/metis.h"
                      (("^//#define[ \t]+IDXTYPEWIDTH[ \t]+32")  "#define IDXTYPEWIDTH 64" )
                      (("^//#define[ \t]+REALTYPEWIDTH[ \t]+32") "#define REALTYPEWIDTH 64" ))
                     ;;patch: CMAKELists.txt
                     (substitute*
                      "CMakeLists.txt"
                      (("build/xinclude") "include"))
                     ;;delete the freaking insufferable make file
                     (delete-file "Makefile")))
        (replace   'configure
                   (lambda* (#:key inputs outputs #:allow-other-keys)
                     (let (
                           (source-dir    (getcwd))
                           (build-dir      "build-dir")
                           (install-prefix (assoc-ref outputs "out"))
                           (gklib-path     (assoc-ref inputs  "gklib"))
                           )
                       (mkdir-p build-dir)
                       (chdir   build-dir)
                       (invoke "cmake"
                               source-dir
                               "-DCMAKE_VERBOSE_MAKEFILE=1"
                               (string-append "-DCMAKE_INSTALL_PREFIX=" install-prefix)
                               (string-append "-DGKLIB_PATH=" gklib-path)

                               ))))
        (delete    'check))))))

(define-public parmetis
  (package
   (name "parmetis")
   (version "4.0.3")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/KarypisLab/ParMETIS.git")
           (commit "8ee6a372ca703836f593e3c450ca903f04be14df")))
     (sha256 (base32 "1hnr3zsa4caysdmrl5sk6ldwhhddh7b2dxy1i0kibf6ppv58pm1g"))))
   (build-system cmake-build-system)
   (native-inputs (list gnu-make gcc perl))
   (inputs (list openmpi gklib metis32))
   (arguments
    (list
     #:phases
     #~(modify-phases
        %standard-phases
        (delete  'check)
        (delete  'validate-runpath)
        (replace 'configure
                 (lambda* (#:key inputs outputs #:allow-other-keys)
                   (let ((install-prefix (assoc-ref outputs "out")))
                     (invoke "make" "config" "shared=1 " "cc=mpicc" (string-append "prefix=" install-prefix)))))
        (replace 'build (lambda _ (invoke "make"))))))
   (license (list license:public-domain license:zlib))
   (home-page "https://exampsle.com")
   (synopsis "ParMETIS is an MPI-based library for partitioning graphs,
partitioning finite element meshes, and producing fill reducing orderings
for sparse matrices.")
   (description "ParMETIS is an MPI-based library for partitioning graphs,
 partitioning finite element meshes, and producing fill reducing orderings
for sparse matrices. The algorithms implemented in ParMETIS are based on
the multilevel recursive-bisection, multilevel k-way, and multi-constraint
partitioning schemes developed in our lab.")))

(define-public fenicsx-dolfinx
  (package
   (name "fenicsx-dolfinx")
   (version "0.10.0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/FEniCS/dolfinx.git")
           (commit  "ac919887a9c9e4714e83261842d9cc4a96c73f21")))
     (file-name (git-file-name name version))
     (sha256
      (base32 "0gvb6656g52lr48pk6iz8rmgxidr3xvsxh0p112940grx5nyqzpc"))))
   (build-system cmake-build-system)
   (native-inputs
    (list cmake
          ninja
          gnu-make
          gcc
          perl
          pkg-config))
   (propagated-inputs
    (list python-fenicsx-ffcx petsc-openmpi slepc-openmpi))
   (inputs
    (list python
          metis32
          parmetis
          gklib
          metis
          fenicsx-basix
          boost
          openmpi
          hdf5-parallel-openmpi
          spdlog
          pugixml))
   (arguments
    (list
     #:configure-flags
     #~(list "CMAKE_VERBOSE_MAKEFILE:BOOL=ON"
             "CMAKE_BUILD_TYPE=Release")
     #:phases
     #~(modify-phases
        %standard-phases
        (delete 'check)
        (replace 'configure
                 (lambda* (#:key inputs outputs #:allow-other-keys)
                   (mkdir "build-dir")
                   (invoke "cmake"
                           (string-append "-DCMAKE_INSTALL_PREFIX="  (assoc-ref outputs "out"))
                           "-DCMAKE_BUILD_TYPE=Release"
                           "-B" "build-dir"
                           "-S" "cpp")))
        (replace 'build
                 (lambda _
                   (invoke "cmake" "--build" "build-dir")))
        (replace 'install
                 (lambda _
                   (invoke "cmake" "--install" "build-dir"))))))
   (license (list license:lgpl3+))
   (home-page "https://fenicsproject.org")
   (synopsis  "Next generation FEniCS problem solving environment")
   (description "DOLFINx is the computational environment of FEniCSx
and implements the FEniCS Problem Solving Environment in C++ and Python.")))

(define-public python-fenicsx-dolfinx
  (package
   (name "python-fenicsx-dolfinx")
   (version "0.10.0")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/FEniCS/dolfinx.git")
       (commit  "ac919887a9c9e4714e83261842d9cc4a96c73f21")))
     (file-name (git-file-name name version))
     (sha256
      (base32 "0gvb6656g52lr48pk6iz8rmgxidr3xvsxh0p112940grx5nyqzpc"))))
   (build-system pyproject-build-system)
   (native-inputs
    (list cmake
          ninja
          gnu-make
          gcc
          perl
          pkg-config
          python-scikit-build-core
          python-nanobind-v2.5
          ))
   (propagated-inputs
    (list
     python
     python-numpy
     python-cffi
     python-mpi4py
     python-fenicsx-basix
     python-fenicsx-ffcx
     python-fenicsx-ufl
     ;; testing
     python-pytest
     python-scipy
     python-matplotlib
     ))
   (inputs
    (list
     boost
     openmpi
     petsc-openmpi
     slepc-openmpi
     spdlog
     pugixml
     hdf5-parallel-openmpi
     fenicsx-dolfinx))
   (arguments
    (list
     #:phases
     #~(modify-phases
        %standard-phases
        (delete 'check)
        (delete 'sanity-check)
        (add-after 'unpack 'change-directory
                   (lambda _ (chdir "python"))))))
   (license license:lgpl3+)
   (home-page   "https://fenicsproject.org")
   (synopsis    "Next generation FEniCS problem solving environment")
   (description "DOLFINx is the computational environment of FEniCSx
and implements the FEniCS Problem Solving Environment in C++ and Python.")))
