;; -*- mode: scheme; fill-column: 80; comment-column: 50; -*-
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Copyright © 2025 Aron Gile <aronggile@gmail.com>

(define-module (dc fenicsx ffcx)
  #:use-module (dc fenicsx basix)
  #:use-module (dc fenicsx ufl)
  #:use-module (gnu packages)
  #:use-module (gnu packages check)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-xyz)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1))


(define-public fenicsx-ffcx
  (package
   (name "fenicsx-ffcx")
   (version "0.10.0.dev0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/FEniCS/ffcx.git")
           (commit "c6aa5cbc8c2360b728c901f1f4e1f04f9ca377b5")))
     (file-name (git-file-name name version))
     (sha256 (base32 "188927rqra57mls512nyk1s3in473q3q5jw5964frrvxz5l37qci"))))
   (build-system cmake-build-system)
   (arguments
    (list
     #:phases
     #~(modify-phases
        %standard-phases

        (replace
         'configure
         (lambda* (#:key outputs #:allow-other-keys)
           (mkdir "build-dir")
           (invoke "cmake"
                   (string-append
                    "-DCMAKE_INSTALL_PREFIX=" (assoc-ref outputs "out"))
                   "-B" "build-dir"
                   "-S" "cmake")
           (invoke "cmake" "--install" "build-dir")))

        (replace 'build
                 (lambda* (#:key outputs #:allow-other-keys)
                   (invoke "cmake" "--build"  "build-dir")))
        (replace 'install
                 (lambda* (#:key outputs #:allow-other-keys)
                   (invoke "cmake" "--install"  "build-dir"))))))

   (license license:public-domain)
   (home-page "https://www.fenicsproject.org")
   (synopsis  "This package provides the UFL language
complier interface, ufcx.h.")
   (description "This package provides ufcx.h header
file for finite element kernels used by DOLFINX
package.")))

(define-public python-fenicsx-ffcx
  (package
   (name "python-fenicsx-ffcx")
   (version "0.10.0.dev0")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/FEniCS/ffcx.git")
           (commit "c6aa5cbc8c2360b728c901f1f4e1f04f9ca377b5")))
     (file-name (git-file-name name version))
     (sha256 (base32 "188927rqra57mls512nyk1s3in473q3q5jw5964frrvxz5l37qci"))))
   (build-system pyproject-build-system)
   (native-inputs
    (list cmake
          gcc
          python-wheel
          python-setuptools
          fenicsx-ffcx
          ;;testing
          ;;python-pytest
          ;;python-sympy
          ;;python-numba
          ))
   (propagated-inputs
    (list python-numpy
          python-cffi
          python-fenicsx-basix
          python-fenicsx-ufl))
   (arguments
    (list #:tests? #f
          #:phases
          #~(modify-phases
             %standard-phases
             (delete 'check)
             (delete 'sanity-check))))
   (license (list license:lgpl3+ license:public-domain))
   (home-page "https://www.fenicsproject.org")
   (synopsis "Fenicsx/FFCX is a compiler for Unified Form Language(UFL)")
   (description "FFCx is a compiler for finite element variational forms.
From a high-level description of the form in the Unified Form Language (UFL),
it generates efficient low-level C code that can be used to assemble the
corresponding discrete operator (tensor). In particular, a bilinear form may
be assembled into a matrix and a linear form may be assembled into a vector.
FFCx may be used either from the command line (by invoking the ffcx command) or
as a Python module (import ffcx).")))
