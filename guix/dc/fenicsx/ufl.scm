;; -*- mode: scheme; fill-column: 80; comment-column: 50; -*-
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Copyright © 2025 Aron Gile <aronggile@gmail.com>

(define-module (dc fenicsx ufl)
  #:use-module (gnu packages)
  #:use-module (gnu packages check)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-xyz)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1))

(define-public python-fenicsx-ufl
  (package
   (name    "python-fenicsx-ufl")
   (version "2024.3.0.dev0")
   (source
    (origin (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/FEniCS/ufl.git")
                  (commit "31e5be79daa8bdd0eac73a2ec2de80702dc5c9c7")))
            (file-name
             (git-file-name name version))
            (sha256
             (base32 "0d7rzcr7gpy8d93j8m3qa61w0s4cql3bp142p69kkdh0sxccjpcm"))))
   (build-system pyproject-build-system)
   (native-inputs
    (list  python-wheel python-setuptools))
   (propagated-inputs (list python-numpy python-cffi python-pytest))
   (license license:gpl3+)
   (home-page "https://fenicsproject.org/")
   (synopsis "UFL consists of a set of operators and atomic expressions
that can be used to express variational forms and functionals.")
   (description "The Unified Form Language (UFL) is a domain specific language for declaration of finite element discretizations of variational forms.
More precisely, it defines a flexible interface for choosing finite
element spaces and defining expressions for weak forms in a notation
close to mathematical notation.")))
