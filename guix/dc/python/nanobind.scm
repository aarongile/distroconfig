;; -*- mode: scheme; fill-column: 80; comment-column: 50; -*-
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Copyright © 2025 Aron Gile <aronggile@gmail.com>

(define-module (dc python nanobind)
  #:use-module (gnu packages)
  #:use-module (gnu packages check)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-xyz)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system pyproject)
  #:use-module (guix build-system python)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  )

;; [NOTICE] https://github.com/wjakob/nanobind/issues/403
;; python-nanoabind version 2.5 manages its dependecies
;; using git submodule. This makes it dificult to url-fetch its
;; source. As a workaround, the source has to be git fetched with the
;; `recursive` option set to true `t to enable git submodule
;; initialization.
(define-public python-nanobind-v2.5
  (package/inherit
   python-nanobind
   (name "python-nanobind-v2.5")
   (version "2.5.0")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/wjakob/nanobind.git")
                  (commit  "4ccbe6e005fc017652312305f280742da49d3dd5")
                  (recursive? #t)
                  ))
            (file-name (git-file-name name version))
            (sha256
             (base32 "0hga25vpgpr2lc9p5si4mrdz04p3w7h034vcjzqs0f3xfxjalzxh"))))))
